﻿using Firebase.Database;
using Firebase.Database.Query;
using LocationTracker.Interfaces;
using LocationTracker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocationTracker.Helpers
{
    public class UsersHandler<T> : ILocationService<T> 
    {
        FirebaseClient firebase = new FirebaseClient(" https://locationtracker-260614.firebaseio.com/");

        public async Task<List<T>> GetSubscribers<T>()
        {
            return (await firebase
             .Child("User")
             .OnceAsync<T>()).Select(a => a.Object).ToList();

        }

        public async Task SetSubscribers(T t)
        {
            await firebase
              .Child("User")
              .PostAsync(t);
        }

        public async Task UpdateUser<T>(T t) where T : User
        {
            var toUpdatePerson = (await firebase
             .Child("User")
             .OnceAsync<User>()).Where(a => a.Object.Id == t.Id).FirstOrDefault();

            await firebase
              .Child("User")
              .Child(toUpdatePerson.Key)
              .PutAsync(t);
        }
    }
}
