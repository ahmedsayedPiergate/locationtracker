﻿using Firebase.Database;
using LocationTracker.Interfaces;
using LocationTracker.Models;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
//using Firebase.Storage;

namespace LocationTracker.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        ILocationService<dynamic> _LocationService;
        private User _userLocation;
        public User UserLocation
        {
            get {
                return _userLocation;
            }
            set {
                SetProperty(ref _userLocation, value);
            }
        }

        private List<User> usersLocation;
        public List<User> UsersLocations
        {
            get { return usersLocation; }
            set { SetProperty(ref usersLocation, value); }
        }

        public MainPageViewModel(INavigationService navigationService, ILocationService<dynamic> LocationService)
            : base(navigationService)
        {
            Title = "Main Page";
            _LocationService = LocationService;
            GeCurrentLocation();
        }


        

        public async Task<Location> GeCurrentLocation()
        {
            try
            {
                if (!Application.Current.Properties.ContainsKey("Id"))
                {
                    Application.Current.Properties["Id"] = Guid.NewGuid();
                   // var id = Application.Current.Properties["Id"];
                    // do something with id
                }
               
                var location = await Geolocation.GetLocationAsync();

                if (location != null)
                {
                    Console.WriteLine($"Latitude: {location.Latitude}, Longitude: {location.Longitude}");


                    UsersLocations = (await _LocationService.GetSubscribers<User>()).Select(a=>a).Distinct().ToList();


                    if (UsersLocations == null)
                        return location;

                    var userLocation = new User { Id = Application.Current.Properties["Id"].ToString(), Latitiude = location.Latitude, Longitude = location.Longitude };



                    if (UsersLocations.Any(a => a.Id == userLocation.Id))
                    {
                        await _LocationService.UpdateUser(userLocation);
                        return location;
                    }

                   
                    await _LocationService.SetSubscribers(userLocation); 
                              
                    return location;
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
                return null;
            }
            catch (FeatureNotEnabledException fneEx)
            {
                // Handle not enabled on device exception
                return null;
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
                return null;
            }
            catch (Exception ex)
            {
                // Unable to get location
                return null;
            }
            return new Location();
        }
    }
}
