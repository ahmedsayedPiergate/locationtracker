﻿using LocationTracker.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms.Maps;

namespace LocationTracker.Models
{
    public class User : INotifyPropertyChanged
    {
        public string Id { get; set; }
        public double Longitude { get; set; }
        public double Latitiude { get; set; }

        private Position position;
        public Position Position { get
            {
                return new Position(Latitiude, Longitude); }
            set
            {
                value = position;
                OnPropertyChanged("Longitude");
            }
        }

        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
