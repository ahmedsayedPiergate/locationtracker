﻿using LocationTracker.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace LocationTracker.Views
{
    public partial class MainPage : ContentPage
    {
        
        public MainPage()
        {
            InitializeComponent();
        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();

            var Location = await ((MainPageViewModel)this.BindingContext).GeCurrentLocation();

            if (Location != null)
            {
                MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(
                      new Xamarin.Forms.Maps.Position(Location.Latitude, Location.Longitude), Distance.FromMiles(1)));

             return;
            }

               MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(
                      new Xamarin.Forms.Maps.Position(30.0444, 31.2357), Distance.FromMiles(1)));
        }

    }
   
}