﻿using LocationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LocationTracker.Interfaces
{
    public interface ILocationService<T>
    {
        Task<List<T>> GetSubscribers<T>();

        Task SetSubscribers(T t);

        Task UpdateUser<T>(T t) where T : User;
    }
}
